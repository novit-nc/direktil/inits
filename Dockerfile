# ------------------------------------------------------------------------
from mcluseau/golang-builder:1.19.4 as build

# ------------------------------------------------------------------------
from alpine:3.17

entrypoint mksquashfs /layer /layer.squashfs -noappend -comp xz >&2 && base64 </layer.squashfs

run apk add --update squashfs-tools

add layer/ /layer/
copy --from=build /go/bin/ /layer/sbin/
