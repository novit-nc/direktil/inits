module novit.nc/direktil/inits

go 1.19

require (
	github.com/antage/mntent v0.0.0-20141129103236-834970000c6c
	github.com/fsnotify/fsnotify v1.6.0
	github.com/go-ping/ping v1.1.0
	github.com/spf13/cobra v1.6.1
	novit.nc/direktil/pkg v0.0.0-20220221171542-fd3ce3a1491b
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	golang.org/x/net v0.4.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
